# Inclusive Systematic Review Registration Form

This Systematic Review Registration Form is intended as a general-purpose registration form. The form is designed to be applicable to reviews across disciplines (i.e., psychology, economics, law, physics, or any other field) and across review types (i.e., scoping review, review of qualitative studies, meta-analysis, or any other type of review). That means that the reviewed records may include research reports as well as archive documents, case law, books, poems, etc. This form, therefore, is a fall-back for more specialized forms and can be used if no specialized form or registration platform is available.

The OSF repository associated to this project is https://osf.io/by27q/ and the preprint with the form itself is available at https://doi.org/10.31222/osf.io/3nbea.
